import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ComponentManagerService } from 'src/app/service/component-manager.service';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['name', 'residuals', 'rate', 'customDeleteColumn', 'customEditColumn'];

  constructor(
    public cmService: ComponentManagerService,
    public router: Router,
    public fbBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.getPricingData();
  }

  getPricingData() {
    this.dataSource = new MatTableDataSource(this.cmService.getPricing());
    this.dataSource.sort = this.sort;
  }

  editPricing(pricing) {
    this.cmService.dataToUpdate = pricing;
    this.router.navigate(['edit']);
  }

  deletePricing(pricing) {
    if (confirm(`Do you really want to delete '${pricing.name}' ?`)) {
      this.cmService.deleteSinglePricing(pricing.ID);
      this.getPricingData();
    }
  }

}
