import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ComponentManagerService } from 'src/app/service/component-manager.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})

export class AddComponent implements OnInit {

  addForm: FormGroup;
  pricing: any = { name: '', savings: '', residuals: '', rate: '' };

  constructor(
    public cmService: ComponentManagerService,
    public fbBuilder: FormBuilder,
    public router: Router,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.creatForm();
  }

  creatForm() {
    this.addForm = this.fbBuilder.group({
      nameControl: new FormControl('', [Validators.required]),
      savingsControl: new FormControl('', [Validators.required]),
      residualsControl: new FormControl('', [Validators.required]),
      rateControl: new FormControl('', [Validators.required]),
    });
  }

  addPricing() {
    if (this.addForm.valid && this.addForm.dirty) {
      this.cmService.addPricing(this.pricing);
      this.router.navigate(['home']);
      this.snackBar.open('Added Successfully', 'Ok', {
        duration: 3000
      });
    }
  }

}
