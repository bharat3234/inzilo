import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ComponentManagerService } from 'src/app/service/component-manager.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  updateForm: FormGroup;

  constructor(
    public cmService: ComponentManagerService,
    public fbBuilder: FormBuilder,
    public router: Router,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    if (!!this.cmService.dataToUpdate.ID) {
      this.creatForm();
    } else {
      this.router.navigate(['home']);
    }
  }

  creatForm() {
    this.updateForm = this.fbBuilder.group({
      nameControl: new FormControl('', [Validators.required]),
      savingsControl: new FormControl('', [Validators.required]),
      residualsControl: new FormControl('', [Validators.required]),
      rateControl: new FormControl('', [Validators.required]),
    });
  }

  updatePricing() {
    if (this.updateForm.valid && this.updateForm.dirty) {
      this.router.navigate(['home']);
      this.snackBar.open('Updated Successfully', 'Ok', {
        duration: 3000
      });
    }
  }

}
