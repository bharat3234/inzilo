import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ComponentManagerService {

  dataToUpdate: any = { name: '', savings: '', residuals: '', rate: '' };
  pricing = [
    {
      ID: 1,
      name: 'Interchange + .10% and $0.10',
      savings: 1331.33,
      residuals: 261.49,
      rate: 2.29
    },
    {
      ID: 2,
      name: 'Interchange + .15% and $0.10',
      savings: 1298.81,
      residuals: 287.51,
      rate: 2.32
    },
    {
      ID: 3,
      name: 'Interchange + .20% and $0.10',
      savings: 1257.34,
      residuals: 320.68,
      rate: 2.36
    },
    {
      ID: 4,
      name: 'Interchange + .25% and $0.10',
      savings: 1215.87,
      residuals: 353.86,
      rate: 2.40
    },
    {
      ID: 5,
      name: 'Interchange + .30% and $0.10',
      savings: 1174.40,
      residuals: 387.03,
      rate: 2.44
    },
    {
      ID: 6,
      name: '2.50% Flat',
      savings: 929.00,
      residuals: 166.66,
      rate: 2.67
    },
    {
      ID: 7,
      name: '2.60% Flat',
      savings: 846.06,
      residuals: 233.02,
      rate: 2.75
    },
    {
      ID: 8,
      name: '2.75% Flat',
      savings: '721.65',
      residuals: 332.55,
      rate: 2.87
    },
    {
      ID: 9,
      name: '6 Tiered w/Red Debit',
      savings: 526.03,
      residuals: 417.52,
      rate: 3.06
    },
    {
      ID: 10,
      name: '3 Tiered w/Red Debit',
      savings: 492.93,
      residuals: 444.00,
      rate: 3.09
    }
  ];

  constructor() { }

  getPricing() {
    return this.pricing;
  }

  deleteSinglePricing(pricingID) {
    this.pricing = this.pricing.filter((item: any) => item.ID !== pricingID);
  }

  updateSinglePricing(pricing) {
    for (const x of this.pricing) {
      if (x.ID == pricing.ID) {
        x.name = pricing.name;
        x.savings = pricing.savings;
        x.residuals = pricing.residuals;
        x.rate = pricing.rate;
      }
      break;
    }
  }

  addPricing(pricing) {
    this.pricing.push({
      ID: this.pricing.length + 1,
      ...pricing
    })
  }

}
